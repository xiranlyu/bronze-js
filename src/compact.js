// TODO:
//
// Implement a function called `compact`. This function creates an array with
// all falsey values removed. The values `false`, `null`, `0`, `""`, `undefined`,
// and `NaN` are falsey.
//
// You can refer to unit test for more details.
//
// <--start-
function compact (array) {
//  const newArray = [];
  const falsey = [false, null, 0, '', undefined, NaN];
  if (array === null) {
    return null;
  } else if (array === undefined) {
    return undefined;
  } else {
    const newArray = [];
    for (let i = 0; i < array.length; i++) {
      if (falsey.includes(array[i]) === false) {
        newArray.push(array[i]);
      }
    }
    return newArray;
  }
}
// --end-->

export default compact;
