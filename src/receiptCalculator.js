class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (id) {
    let totalPrice = 0;
    let currentPrice = 0;
    if (id.length === 0) {
      return 0;
    } else {
      for (let i = 0; i < id.length; i++) {
        for (let j = 0; j < this.products.length; j++) {
          if (id[i] === this.products[j].id) {
            totalPrice += this.products[j].price;
          }
        }
        if (currentPrice === totalPrice) {
          throw Error('Product not found.');
        } else {
          currentPrice = totalPrice;
        }
      }
    }
    return totalPrice;
  }
}
// --end->
export default RecepitCalculator;
